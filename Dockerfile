FROM golang:latest

WORKDIR /usr/src/app/

COPY helloworld/* /usr/src/app/

CMD ["go", "run", "main.go"] 
